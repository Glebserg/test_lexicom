CREATE TABLE IF NOT EXISTS short_names (
    name TEXT NOT NULL,
    status INT NOT NULL
);

TRUNCATE TABLE short_names;

INSERT INTO short_names (name, status)
SELECT 'nazvanie' || generate_series(1, 700000)::TEXT,
       (random() * 1)::INT;
