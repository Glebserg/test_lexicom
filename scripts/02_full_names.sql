CREATE TABLE IF NOT EXISTS full_names (
    name TEXT NOT NULL,
    status INT
);

TRUNCATE TABLE full_names;

INSERT INTO full_names (name, status)
SELECT 'nazvanie' || generate_series(100001, 600000)::TEXT ||
       CASE WHEN (random() * 2)::INT = 0 THEN '.mp3' ELSE '.mpg4' END,
       NULL::INT;
