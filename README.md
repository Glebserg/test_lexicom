# Test Lexicom

## Task 1
```
"""
Вопрос практический. Решение завернутое в докер будет преимуществом. 
Также в зачет идут любые толковые комментарии и интерпретации ручек 
относительно реализации этого сервиса. Необходимо разработать RESTful 
сервис с использованием Fast
API и Redis.
Эндпоинты (ручки)
1. Write_data (запись и обновление данных)
a. Phone
b. Address
2. Check_data (получение данных)
a. Phone
"""
```

# How to run task 1

- [ ]  Clone the repository:
```bash
git clone git@gitlab.com:Glebserg/test_lexicom.git
```
- [ ] Copy* `.env.example` to `.env`
```bash
cp .env.example .env
```
> (*) - if on Windows, then
```bash
copy .env.example .env
```
Edit the file if you needed (e.g., choose ports)
- [ ] Launch command
```bash
docker compose -f docker-compose-task1.yml up --build
```
After the launch, visit [127.0.0.1:8080/docs](http://127.0.0.1:8080/docs) `(localhost:${API_REDIS_PORT}/docs)` 
to view the Swagger schema.

### Summary task 1
Two containers will be launched: api, redis in one Docker network and will communicate using container names.
And you can interact with the API.


## Task 2
```
"""
В одной таблице хранятся имена файлов без расширения. В другой хранятся имена файлов с
расширением. Одинаковых названий с разными расширениями быть не может, количество
расширений не определено, помимо wav и mp3 может встретиться что угодно.

Нам необходимо минимальным количеством запросов к СУБД перенести данные о статусе из
таблицы short_names в таблицу full_names.
"""
```

### my SQL queries
```
CREATE INDEX idx_short_names_name ON short_names(name);
CREATE INDEX idx_full_names_name ON full_names(name);

UPDATE full_names
SET status = short_names.status
FROM short_names
WHERE LEFT(full_names.name, POSITION('.' IN full_names.name) - 1) = short_names.name;
```

### result
```
UPDATE 500000

Query returned successfully in 11 secs 435 msec.
```

## also you can launch api for task 2 and check result
# How to run task 2

```bash
docker compose -f docker-compose-task2.yml up --build
```
After the launch, visit [127.0.0.1:9090/docs](http://127.0.0.1:9090/docs) `(localhost:${API_POSTGRES_PORT}/docs)`
to view the Swagger schema.

### Summary task 2
Two containers will be launched: api, postgres in one Docker network and will communicate using container names.
700000 records will be added in first table and 500000 in second table. And rout for migrate data will be available in 
API. It displays on the screen result: status code and process time.