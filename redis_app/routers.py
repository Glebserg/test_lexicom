from fastapi import APIRouter, HTTPException, status

from redis_app.connector import get_redis, close_redis
import redis_app.models as m


router = APIRouter(tags=["Redis"])


@router.post(
    "/write_data",
    response_model=m.Client,
    status_code=status.HTTP_201_CREATED,
    description="Client registration.",
)
async def write_data(data: m.CreateClientCommand) -> m.Client:
    redis = await get_redis()
    try:
        if await redis.exists(data.phone):
            raise m.ClientAlreadyExists()
        await redis.set(data.phone, data.address)
    except HTTPException as e:
        raise e
    await close_redis(redis)
    return m.Client(phone=data.phone, address=data.address)


@router.put(
    "/write_data",
    response_model=m.Client,
    status_code=status.HTTP_200_OK,
    description="Change client's dataset by phone number.",
)
async def update_data(data: m.UpdateClientCommand) -> m.Client:
    redis = await get_redis()
    try:
        if not await redis.exists(data.phone):
            raise m.ClientNotFound()
        await redis.set(data.phone, data.address)
    except HTTPException as e:
        raise e
    await close_redis(redis)
    return m.Client(phone=data.phone, address=data.address)


@router.get(
    "/check_data",
    response_model=m.Client,
    status_code=status.HTTP_200_OK,
    description="Check client's dataset by phone number.",
)
async def check_data(phone: str) -> m.Client:
    redis = await get_redis()
    try:
        address = await redis.get(phone)
        if address is None:
            raise m.ClientNotFound()
    except HTTPException as e:
        raise e
    await close_redis(redis)
    return m.Client(phone=phone, address=address)
