from fastapi import HTTPException, status

__all__ = [
    "ClientAlreadyExists",
    "ClientNotFound",
]


class ClientAlreadyExists(HTTPException):
    def __init__(self):
        super().__init__(status_code=status.HTTP_400_BAD_REQUEST, detail="Client with this phone already exists")


class ClientNotFound(HTTPException):
    def __init__(self):
        super().__init__(status_code=status.HTTP_404_NOT_FOUND, detail="Client not found")
