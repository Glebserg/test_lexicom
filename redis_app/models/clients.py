from pydantic import BaseModel
from pydantic.fields import Field

__all__ = [
    "Client",
    "ClientFields",
    "CreateClientCommand",
    "UpdateClientCommand",
]


class ClientFields:
    phone = Field(description="Client's number", example="89999999999")
    address = Field(description="Client's address", example="address")


class Client(BaseModel):
    phone: str = ClientFields.phone
    address: str = ClientFields.address


class CreateClientCommand(BaseModel):
    phone: str = ClientFields.phone
    address: str = ClientFields.address


class UpdateClientCommand(BaseModel):
    phone: str = ClientFields.phone
    address: str = ClientFields.address
