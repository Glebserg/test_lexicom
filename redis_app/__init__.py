import os

from dotenv import load_dotenv
from fastapi import FastAPI, Depends, HTTPException, Security, status
from fastapi.security import APIKeyHeader

from redis_app.routers import router


load_dotenv()


def create_app() -> FastAPI:
    app = FastAPI()

    x_api_key_header = APIKeyHeader(name="X-ACCESS-TOKEN", auto_error=False)

    async def get_x_token_key(api_key_header: str = Security(x_api_key_header)):
        """Get X-ACCESS-TOKEN from header and compare it with X_API_TOKEN from
        env.

        This function is used for authentication.

        Args:
            api_key_header: X-ACCESS-TOKEN from header.

        Raises:
            HTTPException: If X-ACCESS-TOKEN from header not equal
                X_API_TOKEN from env.

        Returns: None
        """
        if api_key_header != get_api_token():
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Invalid X-ACCESS-TOKEN",
            )

    app.include_router(router, dependencies=[Depends(get_x_token_key)])

    return app


def get_api_token():
    return os.getenv("API_TOKEN")
