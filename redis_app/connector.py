import os

import aioredis
from dotenv import load_dotenv

load_dotenv()


async def get_redis():
    try:
        redis_host = os.getenv("REDIS_HOST")
        redis_password = os.getenv("REDIS_PASSWORD")
        redis = await aioredis.from_url(f"redis://:{redis_password}@{redis_host}")
        return redis
    except Exception as e:
        raise e


async def close_redis(redis):
    try:
        await redis.close()
    except (ConnectionError, AttributeError) as e:
        raise e
