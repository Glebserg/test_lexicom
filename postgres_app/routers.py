from fastapi import APIRouter, status

from postgres_app.connector import create_pool
from postgres_app.models.timer import timing_decorator
from postgres_app.queries import create_indexes, migrate_data


router = APIRouter(tags=["Postgres"])


@router.get(
    "/move_status",
    status_code=status.HTTP_200_OK,
    description="Move status to full_name from short_name.",
)
@timing_decorator
async def move_status():
    pool = await create_pool()

    try:
        await create_indexes(pool)
        await migrate_data(pool)
    finally:
        await pool.close()

    return {"message": "Data migration successful"}
