import time

__all__ = [
    'timing_decorator',
]

total_db_time = 0


def timing_decorator(func):
    async def wrapper():
        global total_db_time
        start_time = time.time()
        result = await func()
        elapsed_time = time.time() - start_time
        total_db_time += elapsed_time
        return {"message": "Data migration successful", "elapsed_time": elapsed_time}
    return wrapper
