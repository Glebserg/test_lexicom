import os

import asyncpg

postgres_user = os.getenv("POSTGRES_USER")
postgres_password = os.getenv("POSTGRES_PASSWORD")
postgres_db = os.getenv("POSTGRES_DB")
host_name = os.getenv("POSTGRES_SERVICE_NAME")

DATABASE_URL = f"postgresql://{postgres_user}:{postgres_password}@{host_name}:5432/{postgres_db}"


async def create_pool():
    return await asyncpg.create_pool(DATABASE_URL)


async def execute_query(pool, query):
    async with pool.acquire() as connection:
        return await connection.execute(query)
