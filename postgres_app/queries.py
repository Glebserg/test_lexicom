from asyncio import gather
from postgres_app.connector import execute_query


async def create_indexes(pool):
    queries = [
        "CREATE INDEX IF NOT EXISTS idx_short_names_name ON short_names(name)",
        "CREATE INDEX IF NOT EXISTS idx_full_names_name ON full_names(name)",
    ]
    await gather(*[execute_query(pool, query) for query in queries])


async def migrate_data(pool):
    query = """
        UPDATE full_names
        SET status = short_names.status
        FROM short_names
        WHERE LEFT(full_names.name, POSITION('.' IN full_names.name) - 1) = short_names.name
    """
    await execute_query(pool, query)
